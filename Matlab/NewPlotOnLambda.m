function NewPlotOnLambda(x,y,x1,y1,nameFile)
    

    
    plot(x,y,'-o')
    hold on
    plot(x1,y1,'-x')
    lgd = legend('5-fold CV','10-fold CV');
    lgd.FontSize = 15;
    hold off
    h=gcf;
    xlabel('Valores de Lambda','FontSize',12,'FontWeight','bold') ;
    ylabel('RMSE','FontSize',12,'FontWeight','bold') ;
    set(h,'PaperOrientation','landscape');
    set(h,'PaperUnits','normalized');
    set(h,'PaperPosition', [0 0 1 1]);
    print(gcf, '-dpdf', nameFile);
    close;
    
   
    
end
