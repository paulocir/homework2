function RMSE_RSQUARED = K_fold_CV(dataX,dataY,K)
    
    one = ones(size(dataX,1));
    RowOnes = one(:,1);
    dataX = [RowOnes dataX];
    SampleSize = size(dataX,1);
    SamplePart = fix(SampleSize/K);
    aux = SamplePart;
    intervals1 = [];
    intervals2 = [];
    
    for i = 1:K
        
        if i==K
            intervals1 = [intervals1 ((aux-SamplePart)+1)]; %#ok<AGROW>
            intervals2 = [intervals2 (size(dataX,1))];%#ok<AGROW>
        else
            intervals1 = [intervals1 ((aux-SamplePart)+1)]; %#ok<AGROW>
            intervals2 = [intervals2 (SamplePart*i)];%#ok<AGROW>    
        end
        aux = aux +SamplePart;         
        
    end
    RMSE_array =[];
    R_SQUARED_array=[];
    TrainMatrix = [];
    TrainY = [];

    for i = 1:K
        
        TestX = dataX(intervals1(i):intervals2(i),:);
        TestY = dataY(intervals1(i):intervals2(i),:);
        
        for z=1:K
            if z~=i
               TrainMatrix = [TrainMatrix; dataX(intervals1(z):intervals2(z),:)];%#ok<AGROW>
               TrainY = [TrainY;dataY(intervals1(z):intervals2(z),:)];%#ok<AGROW>
            end
        end
        
        
        %ORDINARY LINEAR REGRESSION
        %treino dos k-1 folds:
        inversa = pinv(TrainMatrix'*TrainMatrix);
        betas = inversa*TrainMatrix'*TrainY;        
        
        %Validation with a fold:
        EstimatedYTest = TestX*betas;
        Residual_Array = TestY - EstimatedYTest;
        %RMSE:
        RMSETest = sqrt(mean((Residual_Array).^2));
        %R^2
        R_SquaredTest = corrcoef(TestY,EstimatedYTest).^2;
        R_SquaredTest = R_SquaredTest(1:1,2);
        
        RMSE_array =[RMSE_array RMSETest];%#ok<AGROW>
        R_SQUARED_array=[R_SQUARED_array R_SquaredTest];%#ok<AGROW>
        
        %clear variables:
        TrainMatrix = [];
        TrainY = [];       
        
        
    end
    
    
    RMSE_RSQUARED = [RMSE_array;R_SQUARED_array]';
    
    

    
end
