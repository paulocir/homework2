clear;clc;

%IMPORTING DATA
solTestX = importdata('solTestX.txt');
solTestXtrans = importdata('solTestXtrans.txt');
solTestY = importdata('solTestY.txt');
solTrainX = importdata('solTrainX.txt');
solTrainXtrans = importdata('solTrainXtrans.txt');
solTrainY = importdata('solTrainY.txt');
makePlot = 0;
solTestX=solTestX.data;
solTestXtrans=solTestXtrans.data;
solTestY=solTestY.data;
solTrainX=solTrainX.data;
solTrainXtrans=solTrainXtrans.data;
solTrainY=solTrainY.data;
PredNames = {'MolWeight',	'NumAtoms',	'NumNonHAtoms',	'NumBonds',	'NumNonHBonds',	'NumMultBonds',	'NumRotBonds',	'NumDblBonds',	'NumAromaticBonds',	'NumHydrogen',	'NumCarbon',	'NumNitrogen',	'NumOxygen',	'NumSulfer',	'NumChlorine',	'NumHalogen',	'NumRings'	,'HydrophilicFactor',	'SurfaceArea1',	'SurfaceArea2'};

%CorrelationPlot

%CorrelacoesTrain = corrcov(cov(solTrainXtrans(:,209:228)));
%CorrelationPlot(CorrelacoesTrain,PredNames);
%CorrelationPlotOutCome(solTrainXtrans(:,209:228),solTrainY,PredNames);
%data-preprocessing:
%MyScatterPlot(solTrainXtrans(:,213),solTrainXtrans(:,211),'NumNonHBonds','NumNonHAtoms','ScatterPxP')
%correlation between predictors
matrix = [solTrainX(:,209:228);solTestX(:,209:228)];
%MatrixCorrelation = corrcoef(matrix);



%ORDINARY LINEAR REGRESSION 

one = ones(size(solTrainXtrans,1));
RowOnes = one(:,1);
trainingData = [RowOnes solTrainXtrans];
inversa = pinv(trainingData'*trainingData);
betas = inversa*trainingData'*solTrainY;

EstimatedYTrain = trainingData*betas;
Residuals = solTrainY - EstimatedYTrain;


%RMSE CALC ON TRAIN:
RMSETrain = sqrt(mean((Residuals).^2));
solTestYMean = mean(solTestY);


%R^2
R_SquaredTrain = corrcoef(solTrainY,EstimatedYTrain).^2;
R_SquaredTrain = R_SquaredTrain(1:1,2);


%Now for test:

oneTest = ones(size(solTestX,1));
RowOnesTest = oneTest(:,1);
TestData = [RowOnesTest solTestXtrans];

EstimatedYTest = TestData*betas;
Residuals = solTestY - EstimatedYTest;
%MyScatterPlot(EstimatedYTest,Residuals,'Y Estimado','Res�duo','ScatterPlotYxResidual.pdf');
%MyScatterPlot(EstimatedYTest,solTestY,'Y Estimado','Y de teste','ScatterPlotYxY.pdf');


%RMSE CALC ON TEST:
RMSETest = sqrt(mean((solTestY - EstimatedYTest).^2))


%R^2
R_SquaredTest = corrcoef(solTestY,EstimatedYTest).^2;
R_SquaredTest = R_SquaredTest(1:1,2)


%209,210
%209,211
 PLOT_REG(solTrainXtrans,solTrainY,solTestXtrans,solTestY,209,219)

%NOW PERFORMANCE WITH K-FOLD CV WITH K=5 AND K=10;

dx = [solTestXtrans;solTrainXtrans];

dy = [solTestY;solTrainY];


measurement1 = K_fold_CV(solTrainXtrans,solTrainY,5);
measurement2 = K_fold_CV(solTrainXtrans,solTrainY,10);

measurement3 = K_fold_CVLRTest(TestData,solTestY,5,betas);
measurement4 = K_fold_CVLRTest(TestData,solTestY,10,betas);

Min_tenRMSE = min(measurement4(:,1));
Min_fiveRMSE = min(measurement3(:,1));


phrase = 'NOW FOR RIDGE. Define lambda first'
%RIGDE REGRESSION:
phrase = 'For K = 5';
Five_fold = K_fold_CV_Ridge(trainingData,solTrainY,5)
%PlotOnLambda(Five_fold(3,:),Five_fold(1,:),'Lambda','RMSE M�dio 5-Fold','Lambda5fold.pdf');
phrase = 'For K = 10';
Ten_fold = K_fold_CV_Ridge(trainingData,solTrainY,10)
%PlotOnLambda(Ten_fold(3,:),Ten_fold(1,:),'Lambda','RMSE M�dio 10-Fold','Lambda10fold.pdf');
NewPlotOnLambda(Five_fold(3,:),Five_fold(1,:),Ten_fold(3,:),Ten_fold(1,:),'Lambda10And5fold.pdf')
phrase = 'TESTING WITH LAMBDA SELECTED'
MinValueF = min(Five_fold(1,:));
MinF = find(Five_fold(1,:) == min(Five_fold(1,:)));

MinValueT = min(Ten_fold(1,:));
MinT = find(Ten_fold(1,:) == min(Ten_fold(1,:)));

LambdaChoosed = Ten_fold(3,MinT:MinT);

PLOT_RIDGE(solTrainXtrans,solTrainY,solTestXtrans,solTestY,209,219,LambdaChoosed)
%Now for lambda defined:

RidgeRMSERSQ = RidgeRegression(trainingData,solTrainY,TestData,solTestY,LambdaChoosed);


%PLS E PCR:
%PCR:
choosecomp1 = PCR_K_Fold(solTrainXtrans,solTrainY,5);
choosecomp2 = PCR_K_Fold(solTrainXtrans,solTrainY,10);
x= 1;
plot(choosecomp1(3,:),choosecomp1(1,:),'-o')
hold on
plot(choosecomp2(3,:),choosecomp2(1,:),'-x')

lgd = legend('5-fold CV','10-fold CV');
lgd.FontSize = 15;
hold off
h=gcf;
xlabel('Componentes','FontSize',12,'FontWeight','bold') ;
ylabel('RMSE','FontSize',12,'FontWeight','bold') ;
set(h,'PaperOrientation','landscape');
set(h,'PaperUnits','normalized');
set(h,'PaperPosition', [0 0 1 1]);
print(gcf, '-dpdf', 'PCRFiveandTen.pdf');
close;

MinValueComp = min(choosecomp2(1,:));
MinComp = find(choosecomp2(1,:) == min(choosecomp2(1,:)));

%NOW TEST
ComponentsChoosed = choosecomp2(3,MinComp:MinComp);

PerformancePCR = PCR_TEST(solTrainXtrans,solTrainY,solTestXtrans,solTestY,58);
%2 3
PLOT_PCR(solTrainXtrans,solTrainY,solTestXtrans,solTestY,2,5);

