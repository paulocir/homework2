function CorrelationPlotOutCome(X,Y,L)
    N = size(X,2);
    sz=40;
    aux = 1;
    for i=1:4
        
  
    subplot(2,2,i)  
    scatter(X(:,aux),Y,sz,'MarkerEdgeColor',[0 .5 .5],'MarkerFaceColor',[0 .7 .7],'LineWidth',1.5)
    
    h=gcf;
    xlabel(L(aux),'FontSize',3,'FontWeight','bold') ;
    ylabel('outcome','FontSize',3,'FontWeight','bold') ;
    set(h,'PaperOrientation','landscape');
    set(h,'PaperUnits','normalized');
    set(h,'PaperPosition', [0 0 1 1]);
    aux = aux+5;
    
    end
    
    
    outcome = 'Allplot.pdf';     
   
    print(gcf, '-dpdf', outcome);

    close
   
    
end
