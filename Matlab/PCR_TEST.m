function RMSE_RSQUARED = PCR_TEST(dataX,dataY,dataTestX,dataTestY,NComponents)  
    [coeff,score,latent] = pca(dataTestX);
    dataTestX = score;
    [coeff,score,latent] = pca(dataX);
    dataX = score;    

    one = ones(size(dataX,1));
    RowOnes = one(:,1);
    dataX = [RowOnes dataX];
    
    one = ones(size(dataTestX,1));
    RowOnes = one(:,1);
    dataTestX = [RowOnes dataTestX];     
    

    TrainMatrix1 = dataX(:,1:NComponents+1);             
    TestMatrix1 = dataTestX(:,1:NComponents+1);             

    inversa = pinv(TrainMatrix1'*TrainMatrix1);  
    betas = inversa*TrainMatrix1'*dataY;

    EstimatedYTrain = TestMatrix1*betas;
    Residual_Array = dataTestY - EstimatedYTrain;

    RMSETest = sqrt(mean((Residual_Array).^2));

    R_SquaredTest = corrcoef(dataTestY,EstimatedYTrain).^2;
    R_SquaredTest = R_SquaredTest(1:1,2);      
    
    RMSE_RSQUARED = [RMSETest R_SquaredTest]';
    

    
end
