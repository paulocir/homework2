function RMSE_RSQUARED = RidgeRegression(TrainX,TrainY,TestX,TestY,Lambda)
    
 
            mult = (TrainX'*TrainX);
            identidade = eye(size(TrainX,2));

            ridge = identidade.*Lambda;
            sumOf = mult + ridge;
            
            inversa = pinv(sumOf);  
            betas = inversa*TrainX'*TrainY;
            
            EstimatedYTrain = TestX*betas;
            Residual_Array = TestY - EstimatedYTrain;
            
            RMSETrain = sqrt(mean((Residual_Array).^2));
            
            R_SquaredTest = corrcoef(TestY,EstimatedYTrain).^2;
            R_SquaredTest = R_SquaredTest(1:1,2);  
            
            RMSE_RSQUARED = [RMSETrain R_SquaredTest];    
           
    
end
