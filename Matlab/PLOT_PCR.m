function PLOT_PCR(dataX,dataY,dataTestX,dataTestY,Col1,Col2) 
    

    [coeff,score,latent] = pca(dataX);
    dataX = score; 
    
    dataX = [dataX(:,Col1) dataX(:,Col2)]

    
    one = ones(size(dataX,1));
    RowOnes = one(:,1);
    dataX = [RowOnes dataX];  
    
    
    dataX = [dataX dataX(:,2).*dataX(:,3)];    

    inversa = pinv(dataX'*dataX);  
    b = inversa*dataX'*dataY;
    
    x1= dataX(:,2);
    x2= dataX(:,3);
    y = dataY;
    scatter3(x1,x2,y,'filled')
    hold on
    x1fit = min(x1):2:max(x1);
    x2fit = min(x2):1:max(x2);
    [X1FIT,X2FIT] = meshgrid(x1fit,x2fit);
    YFIT = b(1) + b(2)*X1FIT + b(3)*X2FIT + b(4)*X1FIT.*X2FIT;
    mesh(X1FIT,X2FIT,YFIT)
    xlabel('Componente 1','FontSize',12,'FontWeight','bold')
    ylabel('Componente 2','FontSize',12,'FontWeight','bold') 
    zlabel('Y train','FontSize',12,'FontWeight','bold') 
    view(50,10)
    hold off    
    h=gcf;

    set(h,'PaperOrientation','landscape');
    set(h,'PaperUnits','normalized');
    set(h,'PaperPosition', [0 0 1 1]);
    print(gcf, '-dpdf', 'PlotPCR.pdf');
   
    close;


    

    
end
