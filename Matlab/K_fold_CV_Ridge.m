function RMSE_RSQUARED = K_fold_CV_Ridge(dataX,dataY,K)  
    
    SampleSize = size(dataX,1);
    SamplePart = fix(SampleSize/K);
    aux = SamplePart;
    intervals1 = [];
    intervals2 = [];
    lambdaOptions = [0 0.15 0.25 0.55 0.75 1.0 1.50 2.25 3.5 5.25 6.9 8.25 10 15 20 30 40];
    Tam = size(lambdaOptions,2);
    
    
    for i = 1:K
        
        if i==K
            intervals1 = [intervals1 ((aux-SamplePart)+1)]; %#ok<AGROW>
            intervals2 = [intervals2 (size(dataX,1))];%#ok<AGROW>
        else
            intervals1 = [intervals1 ((aux-SamplePart)+1)]; %#ok<AGROW>
            intervals2 = [intervals2 (SamplePart*i)];%#ok<AGROW>    
        end
        aux = aux +SamplePart;
         
        
    end
    RMSE_array =[];
    R_SQUARED_array=[];
    TrainMatrix = [];
    TrainY = [];

    lambdasArr = [];
    for i = 1:K        
   
        %i=1
        %z=1
        for z=1:K
            if z~=i
               TrainMatrix = [TrainMatrix; dataX(intervals1(z):intervals2(z),:)];%#ok<AGROW>
               TrainY = [TrainY;dataY(intervals1(z):intervals2(z),:)];%#ok<AGROW>
            else
                TestMatrix = dataX(intervals1(z):intervals2(z),:);
                TestY= dataY(intervals1(z):intervals2(z),:);
            end
        end
        
        for z=1:Tam
            mult = (TrainMatrix'*TrainMatrix);
            identidade = eye(size(TrainMatrix,2));


            ridge = identidade.*lambdaOptions(z);
            sumOf = mult + ridge;
            
            inversa = pinv(sumOf);  
            betas = inversa*TrainMatrix'*TrainY;
            
            EstimatedYTrain = TestMatrix*betas;
            Residual_Array = TestY - EstimatedYTrain;
            
            RMSETrain = sqrt(mean((Residual_Array).^2));
            
            R_SquaredTest = corrcoef(TestY,EstimatedYTrain).^2;
            R_SquaredTest = R_SquaredTest(1:1,2);  
            
            RMSE_array =[RMSE_array; RMSETrain];%#ok<AGROW>
            R_SQUARED_array=[R_SQUARED_array; R_SquaredTest];%#ok<AGROW>
            
            
                
            lambdasArr = [lambdasArr; lambdaOptions(z)];%#ok<AGROW>
        end
      
        
        
        
        
        
        %clear variables:
        TrainMatrix = [];
        TrainY = [];       
        
        
    end
    
    auxMatrix = [RMSE_array R_SQUARED_array lambdasArr]';
    
   % auxMatrix = [1 2 3 4 5 6 7 8 9 10;5 9 7 3 2 0 1 5 9 0; 1 1 1 1 1 1 1 1 1 1];
   % auxMatrix = [auxMatrix auxMatrix];
    
    Zeroed = zeros(Tam);
    Zeroed = Zeroed(1:2,:);
    cururu = 1;
    for d=1:K
        lindinha=auxMatrix(1:2,cururu:cururu+Tam-1);
        Zeroed = Zeroed + lindinha;
        cururu = cururu +Tam;
    end
    
    MeanM = Zeroed/K;
    
    MeanM = [MeanM; auxMatrix(3,1:Tam)];
    
    
    
    RMSE_RSQUARED = MeanM;
  
    
    %RMSE_RSQUARED = [RMSE_array R_SQUARED_array lambdasArr]';
    
    

    
end
