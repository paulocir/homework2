function PlotOnLambda(x,y,Xlabel,Ylabel,nameFile)
    

    
    plot(x,y,'-o')
    h=gcf;
    xlabel(Xlabel,'FontSize',12,'FontWeight','bold') ;
    ylabel(Ylabel,'FontSize',12,'FontWeight','bold') ;
    set(h,'PaperOrientation','landscape');
    set(h,'PaperUnits','normalized');
    set(h,'PaperPosition', [0 0 1 1]);
    print(gcf, '-dpdf', nameFile);
    close;
    
   
    
end
