function RMSE_RSQUARED = PCR_K_Fold(dataX,dataY,K)  
    
    [coeff,score,latent,tsquared,explained] = pca(dataX);
    n=size(explained,1);
    y=[1:n]; 
    plot(y,explained,'-o')
    ylim([-5 60])
    h=gcf;
    xlabel('Componentes','FontSize',12,'FontWeight','bold') ;
    ylabel('Percentual explicado','FontSize',12,'FontWeight','bold') ;
    set(h,'PaperOrientation','landscape');
    set(h,'PaperUnits','normalized');
    set(h,'PaperPosition', [0 0 1 1]);
    print(gcf, '-dpdf', 'pcaPercentual1.pdf');
    close;
    
    dataX = score;
    
    one = ones(size(dataX,1));
    RowOnes = one(:,1);
    dataX = [RowOnes dataX];
    
    SampleSize = size(dataX,1);
    SamplePart = fix(SampleSize/K);
    aux = SamplePart;
    intervals1 = [];
    intervals2 = [];
    Components = [1:3:100];
    Tam = size(Components,2);
    
    
    for i = 1:K
        
        if i==K
            intervals1 = [intervals1 ((aux-SamplePart)+1)]; %#ok<AGROW>
            intervals2 = [intervals2 (size(dataX,1))];%#ok<AGROW>
        else
            intervals1 = [intervals1 ((aux-SamplePart)+1)]; %#ok<AGROW>
            intervals2 = [intervals2 (SamplePart*i)];%#ok<AGROW>    
        end
        aux = aux +SamplePart;
         
        
    end
    RMSE_array =[];
    R_SQUARED_array=[];
    TrainMatrix = [];
    TrainY = [];
    CompArr = [];
    for i = 1:K          
  
        for z=1:K
            if z~=i
               TrainMatrix = [TrainMatrix; dataX(intervals1(z):intervals2(z),:)];%#ok<AGROW>
               TrainY = [TrainY;dataY(intervals1(z):intervals2(z),:)];%#ok<AGROW>
            else
                TestMatrix = dataX(intervals1(z):intervals2(z),:);
                TestY= dataY(intervals1(z):intervals2(z),:);
            end
        end
        
        for z=1:Tam
            TrainMatrix1 = TrainMatrix(:,1:Components(z)+1);             
            TestMatrix1 = TestMatrix(:,1:Components(z)+1);             
            
            inversa = pinv(TrainMatrix1'*TrainMatrix1);  
            betas = inversa*TrainMatrix1'*TrainY;
            
            EstimatedYTrain = TestMatrix1*betas;
            Residual_Array = TestY - EstimatedYTrain;
            
            RMSETrain = sqrt(mean((Residual_Array).^2));
            
            R_SquaredTest = corrcoef(TestY,EstimatedYTrain).^2;
            R_SquaredTest = R_SquaredTest(1:1,2);  
            
            RMSE_array =[RMSE_array; RMSETrain];%#ok<AGROW>
            R_SQUARED_array=[R_SQUARED_array; R_SquaredTest];%#ok<AGROW>
            
            
                
            CompArr = [CompArr; Components(z)];%#ok<AGROW>
        end
      
        
        
        
        
        
        %clear variables:
        TrainMatrix = [];
        TrainY = [];       
        
        
    end
    
    auxMatrix = [RMSE_array R_SQUARED_array CompArr]';
    
   % auxMatrix = [1 2 3 4 5 6 7 8 9 10;5 9 7 3 2 0 1 5 9 0; 1 1 1 1 1 1 1 1 1 1];
   % auxMatrix = [auxMatrix auxMatrix];
    
    Zeroed = zeros(Tam);
    Zeroed = Zeroed(1:2,:);
    cururu = 1;
    for d=1:K
        lindinha=auxMatrix(1:2,cururu:cururu+Tam-1);
        Zeroed = Zeroed + lindinha;
        cururu = cururu +Tam;
    end
    
    MeanM = Zeroed/K;
    
    MeanM = [MeanM; auxMatrix(3,1:Tam)];
    
    
    
    RMSE_RSQUARED = MeanM;
  
    
    %RMSE_RSQUARED = [RMSE_array R_SQUARED_array lambdasArr]';

    
end
