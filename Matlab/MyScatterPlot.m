function MyScatterPlot(x,y,Xlabel,Ylabel,nameFile)
    



    sz = 40;
    scatter(x,y,sz,'MarkerEdgeColor',[0 .5 .5],'MarkerFaceColor',[0 .7 .7],'LineWidth',1.5)


    h=gcf;
    xlabel(Xlabel,'FontSize',12,'FontWeight','bold') ;
    ylabel(Ylabel,'FontSize',12,'FontWeight','bold') ;
    set(h,'PaperOrientation','landscape');
    set(h,'PaperUnits','normalized');
    set(h,'PaperPosition', [0 0 1 1]);
    print(gcf, '-dpdf', nameFile);
    close;
    
   
    
end
