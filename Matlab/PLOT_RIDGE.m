function PLOT_RIDGE(dataX,dataY,dataTestX,dataTestY,Col1,Col2,Lambda) 
    

    dataX = [dataX(:,Col1) dataX(:,Col2)];

    
    one = ones(size(dataX,1));
    RowOnes = one(:,1);
    dataX = [RowOnes dataX];  
    
    
    dataX = [dataX dataX(:,2).*dataX(:,3)];    
    identidade = eye(size(dataX,2));

    ridge = identidade.*Lambda;
    inversa = pinv((dataX'*dataX)+ridge);  
    b = inversa*dataX'*dataY;
    
    x1= dataTestX(:,Col1);
    x2= dataTestX(:,Col2);
    y = dataTestY;
    scatter3(x1,x2,y,'filled','MarkerEdgeColor',[.1 .5 .1],'MarkerFaceColor',[.1 1 .1],'LineWidth',1.5)
    hold on
    x1fit = min(x1):0.099:max(x1);
    x2fit = min(x2):0.099:max(x2);
    [X1FIT,X2FIT] = meshgrid(x1fit,x2fit);
    YFIT = b(1) + b(2)*X1FIT + b(3)*X2FIT + b(4)*X1FIT.*X2FIT;
    mesh(X1FIT,X2FIT,YFIT)
    xlabel('MolWeight','FontSize',12,'FontWeight','bold')
    ylabel('NumCarbon','FontSize',12,'FontWeight','bold') 
    zlabel('Y teste','FontSize',12,'FontWeight','bold') 
    view(50,10)
    hold off    
    h=gcf;

    set(h,'PaperOrientation','landscape');
    set(h,'PaperUnits','normalized');
    set(h,'PaperPosition', [0 0 1 1]);
    print(gcf, '-dpdf', 'RidgeRegression.pdf');
    close;


    

    
end
