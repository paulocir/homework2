function CorrelationPlot(M,L)
    n=size(M,1);
    imagesc(M); % plot the matrix
    set(gca, 'XTick', 1:n); % center x-axis ticks on bins
    set(gca, 'YTick', 1:n); % center y-axis ticks on bins  
    
    set(gca, 'XTickLabel', L); % set x-axis labels
    set(gca,'XTickLabelRotation',90)
    
    set(gca, 'YTickLabel', L); % set y-axis labels
    set(gca,'YTickLabelRotation',0)
    title('Correlation', 'FontSize', 14); % set title
    colormap('jet'); % set the colorscheme
    colorbar % enable colorbar
    h=gcf;
 
    set(h,'PaperOrientation','landscape');
    set(h,'PaperUnits','normalized');
    set(h,'PaperPosition', [0 0 1 1]);
    print(gcf, '-dpdf', 'correlationLevel.pdf');

    close
   
    
end
